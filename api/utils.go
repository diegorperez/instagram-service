package api

import (
	"net/http"

	"github.com/pquerna/ffjson/ffjson"
)

type Response struct {
	ContentType    string
	Status         int
	Header         map[string]string
	ErrCode        string
	ErrDescription string
	Payload        []byte
}

type ErrorData struct {
	Error       string `json:"error,omitempty"`
	Description string `json:"description,omitempty"`
}

type SuccessData struct {
	Code        int64  `json:"code"`
	Description string `json:"description"`
}

func (response *Response) Send(w http.ResponseWriter) {
	if response.ContentType == "" {
		response.ContentType = "application/json"
	}
	w.Header().Set("Content-Type", response.ContentType)

	if len(response.Header) > 0 {
		for k, v := range response.Header {
			w.Header().Set(k, v)
		}
	}

	w.WriteHeader(response.Status)

	if response.Status >= http.StatusBadRequest {
		if response.Payload == nil && response.ErrCode != "" {

			var errDesc string
			if response.ErrDescription != "" {
				errDesc = response.ErrDescription
			}

			errData := ErrorData{
				Error:       response.ErrCode,
				Description: errDesc,
			}

			response.Payload = errData.Marshall()
		}
	}

	w.Write(response.Payload)
}

func (errData *ErrorData) Marshall() []byte {
	if b, err := ffjson.Marshal(errData); err == nil {
		return b
	}
	return []byte("")
}

func BuildBadRequestResponse(errDescription string) *Response {
	resp := new(Response)
	resp.Status = http.StatusBadRequest
	resp.ErrCode = ErrorInvalidRequest
	resp.ErrDescription = errDescription

	return resp
}

func BuildInternalErrorResponse(errDescription string) *Response {
	resp := new(Response)
	resp.Status = http.StatusInternalServerError
	resp.ErrCode = ErrorInternalError
	resp.ErrDescription = errDescription

	return resp
}

func BuildNoContentResponse() *Response {
	resp := new(Response)
	resp.Status = http.StatusNoContent

	return resp
}
