package api

import "instagram-service/models"

type ContentRequest struct {
	Type                string     `json:"type"`
	LoadType            string     `json:"load_type"`
	UserID              string     `json:"user_id"`
	Tags                *[]string  `json:"tags"`
	Filter              string     `json:"filter"`
	Description         string     `json:"description,omitempty"`
	TaggedUserNicknames *[]string  `json:"tagged_users_nicknames"`
	SharedIn            *[]string  `json:"share_in"`
	Contents            *[]Content `json:"contents"`
}

type Content struct {
	Data   string  `json:"data"`
	Name   string  `json:"name"`
	Width  float64 `json:"width"`
	Heigth float64 `json:"heigth"`
}

type ContentResponse struct {
	User        *models.User `json:"user"`
	Hashtags    *[]string    `json:"hashtags"`
	Filter      string       `json:"filter"`
	Description string       `json:"description,omitempty"`
	TaggedUsers *[]string    `json:"tagged_users"`
	ContentURLS []string     `json:"content_urls"`
	Comments    int          `json:"comments"`
	Likes       int          `json:"likes"`
}
