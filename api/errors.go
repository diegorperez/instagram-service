package api

const (
	ErrorInvalidRequest = "invalid_request"
	ErrorInternalError  = "internal_error"
)
