package main

import (
	"fmt"
	"instagram-service/core/datasource"
	"instagram-service/logger"
	"instagram-service/routes"
	"instagram-service/settings"
	"net/http"
	"strings"
	"sync"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type HTTPService struct {
	engine    *gin.Engine
	waitGroup *sync.WaitGroup
}

func (service *HTTPService) Init() {

	if debug := settings.Get().App.Debug; !debug {
		gin.SetMode(gin.ReleaseMode)
	}

	service.waitGroup = &sync.WaitGroup{}
	engine := gin.New()
	engine.Use(gin.Recovery())
	engine.Use(gin.Logger())
	engine.Use(cors.Default())

	service.engine = engine
}

func (service *HTTPService) Start() {

	routes.InitRoutes(service.engine)
	port := fmt.Sprintf(":%v", settings.Get().App.HTTPPort)

	server := &http.Server{
		Addr:    port,
		Handler: service.engine,
	}

	go server.ListenAndServe()
	service.waitGroup.Add(1)

	log := logger.GetLogger()
	log.Infof("%s service started!", settings.AppName)
}

func (service *HTTPService) Stop(cause string) {
	log := logger.GetLogger()
	log.Infof("Shutdown requested with signal '%s'", strings.ToUpper(cause))

	datasource.Close()

	log.Infof("%s service is now ready to exit, bye!", settings.AppName)
	service.waitGroup.Done()
}
