package models

import "time"

type Content struct {
	ID           string      `json:"id"`
	Type         string      `json:"type"`
	LoadType     string      `json:"load_type"`
	CreationDate time.Time   `json:"creation_date"`
	Description  string      `json:"description"`
	Filter       string      `json:"filter"`
	Likes        int         `json:"likes"`
	Comments     int         `json:"comments"`
	Contents     *[]Contents `json:"contents,omitempty"`
	User         *User       `json:"user,omitempty"`
	Location     *Location   `json:"location,omitempty"`
	SharedIn     *[]string   `json:"shared_in"`
	Tags         *[]string   `json:"tags,omitempty"`
	TaggedUsers  *[]User     `json:"tagged_users,omitempty"`
}

type Contents struct {
	Urls *URL  `json:"urls,omitempty"`
	Size *Size `json:"size,omitempty"`
}

type URL struct {
	Permalink             string `json:"permalink"`
	ContentURL            string `json:"content_url"`
	ThumbnailURL          string `json:"thumbnail_url"`
	LowResolutionImageURL string `json:"low_resolution_image_url"`
	LowResolutionVideoURL string `json:"low_resolution_video_url"`
}

type Size struct {
	WidthPx  float64 `json:"width_px"`
	HeightPx float64 `json:"height_px"`
}

type User struct {
	ID              string `json:"id"`
	Name            string `json:"name"`
	Nickname        string `json:"nickname"`
	ProfileImageURL string `json:"profile_image_url"`
}

type Location struct {
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"lng"`
}
