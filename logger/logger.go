package logger

import (
	"os"

	log "github.com/Sirupsen/logrus"
)

var logger = log.New()

func Init() {
	log.SetFormatter(&log.TextFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.WarnLevel)
}

func GetLogger() *log.Logger {
	return logger
}
