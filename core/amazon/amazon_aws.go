package amazon

import (
	"instagram-service/core/amazon/s3service"
	"instagram-service/settings"

	"github.com/aws/aws-sdk-go/service/s3"
)

var svc *s3.S3

func Init() (err error) {
	svc, err = s3service.Init(
		settings.Get().Aws.AccessKeyID,
		settings.Get().Aws.SecretAccessKey,
		settings.Get().Aws.Region)

	return
}

func Get() *s3.S3 {
	return svc
}
