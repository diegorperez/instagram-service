package cache

import (
	"instagram-service/settings"

	"github.com/go-redis/redis"
)

var client *redis.Client

func Init() (err error) {
	client = redis.NewClient(&redis.Options{
		Addr:     settings.Get().Redis.Address,
		Password: "",
		DB:       0,
	})

	_, err = client.Ping().Result()

	return
}

func Get() *redis.Client {
	return client
}

func Close() error {
	return client.Close()
}
