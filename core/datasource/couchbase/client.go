package couchbase

import (
	"github.com/couchbase/gocb"
)

func Init(clusterAddress, bucketName, username, password string) (bucket *gocb.Bucket, err error) {
	cluster, err := gocb.Connect(clusterAddress)
	if err != nil {
		return
	}

	cluster.Authenticate(gocb.PasswordAuthenticator{
		Username: username,
		Password: password,
	})

	bucket, err = cluster.OpenBucket(bucketName, "")

	return
}
