package datasource

import (
	"instagram-service/core/datasource/couchbase"
	"instagram-service/settings"

	gocb "github.com/couchbase/gocb"
)

var bucket *gocb.Bucket

func Init() (err error) {
	bucket, err = couchbase.Init(
		settings.Get().Database.Address,
		settings.Get().Database.Bucket,
		settings.Get().Database.Username,
		settings.Get().Database.Password)

	return
}

func Get() *gocb.Bucket {
	return bucket
}

func Close() error {
	return bucket.Close()
}
