package main

import (
	"flag"
	"instagram-service/core/amazon"
	"instagram-service/core/cache"
	"instagram-service/core/datasource"
	"instagram-service/logger"
	"instagram-service/settings"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
)

var (
	settingsFile *string
	configPath   *string
)

func main() {
	loadArguments()
	appInit()
	httpServiceStart()
}

// application start
func appInit() {

	if err := settings.Init(*settingsFile); err != nil {
		log.Fatal(err)
	}

	logger.Init()
	log := logger.GetLogger()

	if errDb := datasource.Init(); errDb != nil {
		log.Fatalf("Error connecting with Couchbase DB Server: %s", errDb.Error())
	}

	if errAws := amazon.Init(); errAws != nil {
		log.Fatalf("Error connecting with Amazon AWS Service: %s", errAws.Error())
	}

	if errCache := cache.Init(); errCache != nil {
		log.Fatalf("Error connecting with Redis DB: %s", errCache.Error())
	}
}

func httpServiceStart() {
	httpService := HTTPService{}
	httpService.Init()

	go httpService.Start()

	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	sign := <-ch

	httpService.Stop(sign.String())
	os.Exit(0)
}

func loadArguments() {
	settingsFile = flag.String("settings", "", "Settings file")
	flag.Parse()

	sFile := *settingsFile
	if sFile != "" {
		if !filepath.IsAbs(sFile) {
			sFile = filepath.Join(getAppPath(), sFile)
			settingsFile = &sFile
		}
	} else {
		sFile = filepath.Join(getAppPath(), "settings.yml")
		settingsFile = &sFile
	}

}

func getAppPath() string {
	if dir, err := filepath.Abs(filepath.Dir(os.Args[0])); err == nil {
		return dir
	}

	return ""
}
