# instagram-service

## Introducci�n

El servicio consiste de una API Rest que contiene la implementaci�n del subsistema de creaci�n de contenidos y tambi�n del subsistema de manejo del feed de los usuarios.
Se utiliza la base de datos Couchbase para almacenar los contenidos y Redis para el feed de los usuarios. Las imagenes y videos son almacenados en un bucket del servicio de S3 de Amazon AWS.

## Ejecuci�n

Para ejecutar la aplicaci�n, se deber� ejecutar el binario correspondiente al sistema operativo sobre el cual estemos trabajando. Los binarios para las distintas plataformas se encuentran dentro del directorio *releases*.
Por ejemplo, para ejecutar en Mac OS, dirigirse al directorio *releases/mac-os/* y ejecutar el binario con el nombre **instagram-service**.
El binario se puede ejecutar a trav�s del siguiente comando:


`bash ./instagram-service`

NOTA: En el directorio donde se ejecuta el binario, es necesario que se encuentre el archivo **settings.yml** el cual contiene los parametros de configuraci�n de la aplicaci�n.

## Configuraci�n de la aplicaci�n

La aplicaci�n cuenta con un archivo llamado **settings.yml** el cual debe estar presente en el directorio donde se ejecuta el binario. Lo �nico que se debe configurar para que la aplicaci�n funcione es el nombre del bucket creado en Couchbase y el username y password asinado al cluster de couchbase para que la aplicaci�n pueda conectarse.
Por defecto, se encuentra configurado el puerto **7070** para el web server que contiene la aplicaci�n. Es posible de modificar el puerto dentro del archivo **settings.yml** a trav�s de la propiedad *http_port* dentro de la propiedad *app*. 

## Configuraci�n de Couchbase

Para almacenar los contenidos creados por los usuarios, es necesario configurar Couchbase. Se explicar� como utilizar Couchbase mediante su ejecuci�n dentro de un container de Docker.

En primer lugar, se deber� instalar el engine de Docker, lo cual se explica en el siguiente link: (https://docs.docker.com/install/).

Luego, se deber� ejecutar el siguiente comando para comenzar a ejecutar Couchbase dentro del contenedor llamado **db**:

`docker run -d --name db -p 8091-8094:8091-8094 -p 11210-11211:11210-11211 couchbase`

Una vez ejecutado el comando, acceder a la consola de Couchbase a trav�s de la URL: (http://localhost:8091). Configurar el cluster como se explica en este link: (https://developer.couchbase.com/documentation/server/current/install/init-setup.html).

Luego de haber configurado correctamente el cluster, se deber� crear un bucket con el nombre **instagram-content** (En el caso de utilizar otro nombre para la creaci�n del bucket, se deber� configurar el mismo en el archivo **settings.yml** en la propiedad *bucket* dentro de la propiedad *database*). Para crear un nuevo bucket, seguir la gu�a (https://developer.couchbase.com/documentation/server/current/clustersetup/create-bucket.html). El bucket deber� ser creado de tipo Couchbase.

## Configuraci�n de Redis

Se deber� ejecutar el siguiente comando para comenzar a ejecutar Redis dentro del contenedor llamado **redis**:

`docker run --name redis -p 6379:6379 -d redis`

En el caso de querer ejecutar comandos utilizando el cli de Redis, ejecutar:

`docker run -it --link redis:redis --rm redis redis-cli -h redis -p 6379:6379`
