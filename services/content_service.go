package services

import (
	"bytes"
	"fmt"
	"instagram-service/api"
	"instagram-service/core/amazon"
	"instagram-service/database"
	"instagram-service/logger"
	"instagram-service/models"
	"instagram-service/settings"
	"math/rand"
	"net/http"
	"path"
	"time"

	"github.com/pquerna/ffjson/ffjson"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/mmcloughlin/spherand"
	uuid "github.com/satori/go.uuid"
	"github.com/vincent-petithory/dataurl"
)

func GetContent(id string) (apiResponse *api.Response) {
	apiResponse = new(api.Response)
	logger := logger.GetLogger()
	content, err := database.GetContent(id)
	if err != nil {
		msg := fmt.Sprintf("Error while trying to retrieve content with id %s", id)
		logger.Error(msg)
		return api.BuildInternalErrorResponse(msg)
	}

	apiResponse.Status = http.StatusOK
	apiResponse.Payload, _ = ffjson.Marshal(content)

	return
}

func GetContents() (apiResponse *api.Response) {
	apiResponse = new(api.Response)
	logger := logger.GetLogger()
	contents, err := database.GetContents()
	if err != nil {
		msg := fmt.Sprintf("Error while trying to retrieve contents. More information: %s", err.Error())
		logger.Error(msg)
		return api.BuildInternalErrorResponse(msg)
	}

	var contentsResponse = []api.ContentResponse{}
	for _, content := range contents {
		taggedUsers := *content.TaggedUsers
		taggedUserNicknames := getTaggedUserNicknames(taggedUsers)
		contentURLS := getContentURLS(*content.Contents)
		contentResponse := api.ContentResponse{
			User:        content.User,
			Filter:      content.Filter,
			Comments:    content.Comments,
			Likes:       content.Likes,
			Description: content.Description,
			Hashtags:    content.Tags,
			TaggedUsers: &taggedUserNicknames,
			ContentURLS: contentURLS,
		}
		contentsResponse = append(contentsResponse, contentResponse)
	}

	apiResponse.Status = http.StatusOK
	apiResponse.Payload, _ = ffjson.Marshal(contentsResponse)

	return
}

func CreateContent(contentDto *api.ContentRequest) (apiResponse *api.Response) {
	apiResponse = new(api.Response)
	content := new(models.Content)
	logger := logger.GetLogger()

	content.ID = createNewUUID()
	content.CreationDate = time.Now()
	content.Description = contentDto.Description
	content.Filter = contentDto.Filter
	content.Type = contentDto.Type
	content.LoadType = contentDto.LoadType
	content.Tags = contentDto.Tags
	content.SharedIn = contentDto.SharedIn
	content.User = getDummyCreatorUser()
	content.Location = getRandomLocation()
	content.Comments = 0
	content.Likes = 0
	taggedUsers := getTaggedUsers(contentDto.TaggedUserNicknames)
	content.TaggedUsers = &taggedUsers

	contents := []models.Contents{}
	for _, c := range *contentDto.Contents {
		cs := models.Contents{}
		contentURL, err := uploadContent(c.Name, c.Data)
		if err != nil {
			msg := fmt.Sprintf("Error while trying to upload image to s3. More info: %s", err.Error())
			logger.Error(msg)
			return api.BuildInternalErrorResponse(msg)
		}
		url := new(models.URL)
		url.ContentURL = contentURL
		url.LowResolutionImageURL = contentURL
		url.Permalink = contentURL
		url.ThumbnailURL = contentURL

		size := new(models.Size)
		size.HeightPx = c.Heigth
		size.WidthPx = c.Width

		cs.Urls = url
		cs.Size = size

		contents = append(contents, cs)
	}
	content.Contents = &contents

	// Save content in couchbase database
	err := database.StoreContent(content)
	if err != nil {
		msg := fmt.Sprintf("Error while trying to store content %v", content)
		logger.Error(msg)
		return api.BuildInternalErrorResponse(msg)

	}

	apiResponse.Status = http.StatusOK
	apiResponse.Payload, _ = ffjson.Marshal(content)

	// Store content in users feed
	go addContentToFeed(content)

	return
}

func getContentURLS(contents []models.Contents) (contentURLS []string) {
	for _, c := range contents {
		contentURLS = append(contentURLS, c.Urls.ContentURL)
	}

	return
}

func getTaggedUserNicknames(taggedUsers []models.User) (taggedUserNicknames []string) {
	for _, taggedUser := range taggedUsers {
		taggedUserNicknames = append(taggedUserNicknames, taggedUser.Nickname)
	}

	return
}

func getTaggedUsers(taggedUsersNickname *[]string) (taggedUsers []models.User) {
	for _, nickname := range *taggedUsersNickname {
		taggedUsers = append(taggedUsers, *getDummyUserByNickname(nickname))
	}

	return
}

func createNewUUID() string {
	guid, _ := uuid.NewV4()
	return guid.String()
}

func getRandomLocation() (location *models.Location) {
	location = new(models.Location)
	g := spherand.NewGenerator(rand.New(rand.NewSource(42)))
	lat, lng := g.Geographical()

	location.Latitude = lat
	location.Longitude = lng

	return
}

func getSize(width, height float64) *models.Size {
	size := new(models.Size)
	size.WidthPx = width
	size.HeightPx = height

	return size
}

func uploadContent(contentName, contentData string) (contentURL string, err error) {
	svc := amazon.Get()
	currentDate := fmt.Sprintf("%d%d%d%d%d%d", time.Now().Year(), time.Now().Month(), time.Now().Day(), time.Now().Hour(), time.Now().Minute(), time.Now().Second())
	objectName := path.Join(settings.Get().S3.MediaPath, fmt.Sprintf("%s_%s", currentDate, contentName))

	dataURL, err := dataurl.DecodeString(contentData)
	if err != nil {
		err = fmt.Errorf("Error while decoding data url from uploaded image %s", contentName)
		return
	}

	contentBytes := []byte(dataURL.Data)
	contentReader := bytes.NewReader(contentBytes)

	params := &s3.PutObjectInput{
		Bucket:      aws.String(settings.Get().S3.Bucket),
		Key:         aws.String(objectName),
		Body:        contentReader,
		ContentType: aws.String(dataURL.MediaType.ContentType()),
	}

	_, err = svc.PutObject(params)
	if err != nil {
		err = fmt.Errorf("Error while uploading image %s to s3. More info: %s", contentName, err.Error())
		return
	}

	contentURL = path.Join(settings.Get().S3.BaseURL, objectName)

	return
}
