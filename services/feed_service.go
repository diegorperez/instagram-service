package services

import (
	"fmt"
	"instagram-service/api"
	"instagram-service/database"
	"instagram-service/logger"
	"instagram-service/models"
	"net/http"
	"strings"

	"github.com/pquerna/ffjson/ffjson"
)

func GetUserFeed(idUser string) (apiResponse *api.Response) {
	apiResponse = new(api.Response)
	logger := logger.GetLogger()

	feed := []api.ContentResponse{}
	contents, _ := database.GetFeedFromUser(idUser)
	for _, content := range contents {
		var contentResponse = new(api.ContentResponse)
		decoder := ffjson.NewDecoder()
		errDecode := decoder.DecodeReader(strings.NewReader(content), contentResponse)
		if errDecode != nil {
			errDescription := "The JSON content from the user feed couldn't be decode"
			logger.Errorf(errDescription)
		} else {
			feed = append(feed, *contentResponse)
		}
	}

	apiResponse.Status = http.StatusOK
	apiResponse.Payload, _ = ffjson.Marshal(feed)

	return
}

func addContentToFeed(content *models.Content) {
	logger := logger.GetLogger()
	taggedUsers := getTaggedUserNicknames(*content.TaggedUsers)
	contentURLS := getContentURLS(*content.Contents)
	contentResponse := api.ContentResponse{
		User:        content.User,
		Filter:      content.Filter,
		Comments:    content.Comments,
		Likes:       content.Likes,
		Description: content.Description,
		Hashtags:    content.Tags,
		TaggedUsers: &taggedUsers,
		ContentURLS: contentURLS,
	}

	encodedContent, _ := ffjson.Marshal(contentResponse)
	err := database.AddContentToUserFeed(content.User.ID, string(encodedContent))

	if err != nil {
		msg := fmt.Sprintf("Error while trying to store content in the feed of the user %s", content.User.ID)
		logger.Error(msg)
	}
}
