package services

import (
	"instagram-service/models"
	"strings"
)

const (
	dummyCreatorUserID = "ef152e9b-7442-4bfc-9d4d-dee68b4063d2"
)

var dummyUsers = map[string]*models.User{
	dummyCreatorUserID: &models.User{
		ID:              dummyCreatorUserID,
		Name:            "Test user",
		Nickname:        "test",
		ProfileImageURL: "https://s3.amazonaws.com/instagram-bbdd/empty-profile.png",
	},
}

func getDummyCreatorUser() *models.User {
	return dummyUsers[dummyCreatorUserID]
}

func getDummyUserByNickname(nickname string) *models.User {
	dummyUser := new(models.User)
	dummyUser.ID = createNewUUID()
	dummyUser.Name = strings.ToTitle(nickname)
	dummyUser.Nickname = nickname
	dummyUser.ProfileImageURL = "https://s3.amazonaws.com/instagram-bbdd/empty-profile.png"

	return dummyUser
}
