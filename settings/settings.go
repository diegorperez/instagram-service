package settings

import (
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

const AppName = "Instagram Content Service"

type Settings struct {
	App struct {
		Debug    bool `yaml:"debug"`
		HTTPPort int  `yaml:"http_port"`
	} `yaml:"app"`
	Database struct {
		Address  string `yaml:"address"`
		Bucket   string `yaml:"bucket"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
	} `yaml:"database"`
	Aws struct {
		AccessKeyID     string `yaml:"access_key_id"`
		SecretAccessKey string `yaml:"secret_access_key"`
		Region          string `yaml:"region"`
	} `yaml:"aws"`
	S3 struct {
		Bucket    string `yaml:"bucket"`
		MediaPath string `yaml:"media_path"`
		BaseURL   string `yaml:"base_url"`
	} `yaml:"s3"`
	Redis struct {
		Address string `yaml:"address"`
	} `yaml:"redis"`
}

var cfg *Settings

func Init(file string) (err error) {
	if _, err := os.Stat(file); err == nil {
		err = loadSettingsFromFile(file)
	} else {
		return errors.Errorf("File '%s' not found", file)
	}

	return
}

func Get() (s *Settings) {
	if cfg != nil {
		s = cfg
	}
	return
}

func GetHTTPClient() (client *http.Client) {
	client = http.DefaultClient

	return
}

func loadSettingsFromFile(file string) (err error) {
	cfg = new(Settings)

	fileContent, err := ioutil.ReadFile(file)
	if err != nil {
		return errors.Wrap(err, "Error reading configuration file")
	}

	if err := yaml.Unmarshal(fileContent, cfg); err != nil {
		return errors.Wrap(err, "Error parsing configuration file")
	}

	return
}

func GetAppPath() string {
	if dir, err := filepath.Abs(filepath.Dir(os.Args[0])); err == nil {
		return dir
	}
	return ""
}
