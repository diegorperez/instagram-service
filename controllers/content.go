package controllers

import (
	"instagram-service/api"
	"instagram-service/services"

	"github.com/gin-gonic/gin"
	"github.com/pquerna/ffjson/ffjson"
)

func StoreContent() gin.HandlerFunc {
	return func(c *gin.Context) {
		apiResponse := new(api.Response)
		content := new(api.ContentRequest)

		decoder := ffjson.NewDecoder()
		if errDecode := decoder.DecodeReader(c.Request.Body, content); errDecode != nil {
			errDescription := "The content JSON couldn't be decode"
			apiResponse = api.BuildBadRequestResponse(errDescription)
		} else {
			apiResponse = services.CreateContent(content)
		}

		apiResponse.Send(c.Writer)
	}
}

func GetContent() gin.HandlerFunc {
	return func(c *gin.Context) {
		apiResponse := new(api.Response)

		idContent, exists := c.GetQuery("id")
		if exists {
			apiResponse = services.GetContent(idContent)
		} else {
			apiResponse = api.BuildBadRequestResponse("Id param must be present")
		}

		apiResponse.Send(c.Writer)
	}
}

func GetContents() gin.HandlerFunc {
	return func(c *gin.Context) {
		apiResponse := new(api.Response)

		apiResponse = services.GetContents()

		apiResponse.Send(c.Writer)
	}
}
