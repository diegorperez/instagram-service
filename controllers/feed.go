package controllers

import (
	"instagram-service/api"
	"instagram-service/services"

	"github.com/gin-gonic/gin"
)

func GetFeed() gin.HandlerFunc {
	return func(c *gin.Context) {
		apiResponse := new(api.Response)

		idUser, exists := c.GetQuery("id")
		if exists {
			apiResponse = services.GetUserFeed(idUser)
		} else {
			apiResponse = api.BuildBadRequestResponse("Id param must be present")
		}

		apiResponse.Send(c.Writer)
	}
}
