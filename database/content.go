package database

import (
	"instagram-service/core/datasource"
	"instagram-service/models"

	"github.com/couchbase/gocb"
)

func GetContents() (contents []models.Content, err error) {
	bucket := datasource.Get()

	query := gocb.NewN1qlQuery("SELECT i.* FROM `instagram-content` i")
	rows, err := bucket.ExecuteN1qlQuery(query, nil)
	if err != nil {
		return
	}

	var row models.Content
	for rows.Next(&row) {
		contents = append(contents, row)
	}

	return
}

func StoreContent(content *models.Content) (err error) {
	bucket := datasource.Get()

	_, err = bucket.Insert(content.ID, content, 0)

	return
}

func GetContent(id string) (content models.Content, err error) {
	bucket := datasource.Get()

	_, err = bucket.Get(id, &content)

	return
}
