package database

import (
	"instagram-service/core/cache"
)

func AddContentToUserFeed(idUser, content string) (err error) {
	client := cache.Get()

	_, err = client.LPush(idUser, content).Result()

	return
}

func GetFeedFromUser(idUser string) (contents []string, err error) {
	client := cache.Get()

	len, err := client.LLen(idUser).Result()

	contents, err = client.LRange(idUser, 0, len).Result()

	return
}
