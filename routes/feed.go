package routes

import (
	"instagram-service/controllers"

	"github.com/gin-gonic/gin"
)

func setFeedRoutes(engine *gin.Engine) {
	engine.GET("feed", controllers.GetFeed())
}
