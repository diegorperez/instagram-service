package routes

import (
	"instagram-service/controllers"

	"github.com/gin-gonic/gin"
)

func setContentRoutes(engine *gin.Engine) {
	engine.POST("contents/create", controllers.StoreContent())
	engine.GET("contents/retrieve", controllers.GetContent())
	engine.GET("contents", controllers.GetContents())
}
