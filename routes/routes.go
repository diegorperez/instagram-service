package routes

import "github.com/gin-gonic/gin"

func InitRoutes(engine *gin.Engine) {
	setContentRoutes(engine)
	setFeedRoutes(engine)
}
